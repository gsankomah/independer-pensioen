<footer class="page-footer" itemscope="" itemtype="http://schema.org/WPFooter">
<div class="container">
  <div class="page-footer--information" itemscope="" itemtype="http://schema.org/PostalAddress">
    <span itemprop="name">Independer.nl N.V. h.o.d.n. Independer</span>
    <span itemprop="streetAddress">Snelliuslaan 10</span>
    <span><span itemprop="postalCode">1222 TE</span> <span itemprop="addressLocality">Hilversum</span></span>
    <span>KvKnr. 34117387</span>
    <span>WFT-verg.nr 12012777</span>
    <span>BTW nr. NL8085.81.612.B.01</span>
    <span class="ng-binding">Independer 1999 - 2016</span>
  </div>
  <div class="page-footer--information">
    <span itemprop="name"><a href="https://www.independer.nl/thema/common/dienstverlening.aspx" target="_blank" itemprop="url">Dienstverlening</a></span>
    <span itemprop="name"><a href="https://www.independer.nl/cookie-toestemming.aspx" target="_blank" itemprop="url">Cookiebeleid</a></span>
    <span itemprop="name"><a href="https://www.independer.nl/algemeen/info/privacystatement.aspx" target="_blank" itemprop="url">Privacy</a></span>
    <span itemprop="name"><a href="https://www.independer.nl/thema/common/dienstverlening.aspx#disclaimer" target="_blank" itemprop="url">Disclaimer</a></span>
    <span itemprop="name"><a href="https://www.independer.nl/algemeen/info/responsible-disclosure.aspx" target="_blank" itemprop="url">Responsible Disclosure</a></span>
  </div>
  <div class="text-center l-margin-sm-top l-margin-md-bottom">
    <button class="btn btn-servicecode" id="expertButton" onclick="location.href='mailto:dbrouwer@independer.nl?subject=Hulp%20bij%20Pensioenoplossing%20Independer'">
      <span><span class="if-generic--servicecode-eye inactive"></span>laat expert meekijken</span>
    </button>
  </div>
</div>

</footer>

<script src="content/js/jquery-3.1.0.min.js"></script>
<!-- <script src="content/js/custom-file-input.js"></script> -->
<script type="text/javascript">
  function toggleDiv(divId) {
    $("#"+divId).toggle(100);
  }
</script>
<!-- Hotjar -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:251258,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

</body>
</html>
