<?php
ini_set('session.cache_limiter', 'private');
session_start();

$pageTitle = "Krijg meer inzicht in je pensioen";
include ('header.php');

?>

<div class="view-container" itemscope="" itemtype="http://schema.org/FinancialService">
  <span itemprop="name" content="Independer"></span>
  <div ui-view="">
    <div class="flow-page-header" style="margin-bottom:0;">
      <div class="container">
        <a class="flow-page-header--terug-link link-arrow-back" href="https://www.independer.nl">terug naar Independer.nl</a><!-- end ngIf: flowPageHeaderController.showBackLink() -->
        <h1><span>
          <!-- <?php echo $pageTitle ?> -->
      </h1>
    </div>
  </div>
    <section class="purple-milled-bottom intro-section--salesbox">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <form action="2-aow.php" method="post">
              <div class="intro-section--salesbox--main text-center">
                <div><h1>Eerder stoppen met werken?</h1></div>
                <p>
                  Check of je voldoende inkomen voor later hebt opgebouwd om met pensioen te gaan wanneer jij dat wilt.
                  Kom je geld tekort? Wij helpen je met het maken en uitvoeren van een plan om dat tekort aan te vullen.
                </p>

                <div class="input-horizontal pensioenLeeftijd">
                  <div class="input-item input-item--geboortedatum">
                      <div class="input-item--question">Wat is je geboortedatum?</div>
                      <div class="input-item--answer">
                          <input id="geboortedatum" type="text" required="" class="input-control" name="geboortedatum" placeholder="bijv. 01-01-1990">
                      </div>
                  </div>

                  <div class="input-item input-item--pensioenleeftijd">
                      <div class="input-item--question">Op welke leeftijd wil je met pensioen?</div>
                      <div class="input-item--answer">
                          <input id="gewenstePensioenleeftijd" type="text" required="" class="input-control" name="gewenstePensioenleeftijd" placeholder="bijv. 65">
                      </div>
                  </div>
                </div>
                <button id="cta-eerstestap" class="btn btn-cta" type="submit">naar eerste stap</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <section class="intro-section--zelf-afsluiten" id="videoblock">
      <div class="container text-center">
        <div class="col-xs-12 col-sm-8 col-sm-push-2"><h2>Over de vervolgstappen</h2>
    Om inzichtelijk te maken hoe je jouw gewenste pensioenleeftijd kan bereiken, doorlopen we samen de onderstaande stappen. We beloven je dat het niet moeilijk is om tot het beste plan te komen.</div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-4 text-center">
            <img src="content/images/icons/light.gif" alt="" />
            <h5>1. Inzicht geven</h5>
            <div>We geven je inzicht in hoeveel inkomen je krijgt op jouw gewenste pensioenleeftijd. En of dit genoeg is om dan met pensioen te kunnen gaan.</div>
          </div>
          <div class="col-xs-12 col-sm-4 text-center">
            <img src="content/images/icons/sun.gif" alt="" />
            <h5>2. Plan presenteren</h5>
            <div>Bouw je nog niet genoeg inkomen voor later op om de door jouw gewenste leeftijd met pensioen te gaan? Dan stellen wij een plan voor hoe je dit wel kunt bereiken.</div>
          </div>
          <div class="col-xs-12 col-sm-4 text-center">
            <img src="content/images/icons/check.gif" alt="" />
            <h5>3. Afsluiten</h5>
            <div>Hoe eerder je in actie komt, hoe voordeliger het is om jouw gewenste pensioenleeftijd mogelijk te maken. Daarom helpen we jou bij het regelen en uitvoeren van jouw plan.</div>
          </div>
        </div>
      </div>
    </section>

    <div class="container"><div class="divider"></div></div>

    <!-- <section class="intro-section--zelf-afsluiten" id="videoblock">
      <div class="container text-center">
        <div class="col-xs-12 col-sm-8 col-sm-push-2"><h2>Klanten die via ons pensioenadvies hebben gekregen</h2>
    Beoordelingen zijn door klanten geschreven na het volgen van een pensioenadvies. We tonen alle beoordelingen over ons. Independer scoort een gemiddelde van een 9,1 uit <a href="#" id="reviewsLink">854 beoordelingen</a></div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-4 reviewbox text-center">
            <h5>"Super!"</h5>
            Geschreven op 27 juli 2016
            <div class="reviewStars">
              <div class="stars">
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star half"></li>
              </div>
            </div>
            Handige site werkt super!

          </div>

          <div class="col-xs-12 col-sm-4 reviewbox text-center">
            <h5>"Bedankt voor het overzicht!"</h5>
            Geschreven op 1 augustus 2016
            <div class="reviewStars">
              <div class="stars">
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star half"></li>
                <li class="star"></li>
              </div>
            </div>
            Ik zou zeer tevreden zijn geweest als ik na het invullen van de vragenlijst nog een vak met opmerkingen had kunnen invullen.
          </div>

          <div class="col-xs-12 col-sm-4 reviewbox text-center">
            <h5>"Super!"</h5>
            Geschreven op 27 juli 2016
            <div class="reviewStars">
              <div class="stars">
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star full"></li>
                <li class="star "></li>
              </div>
            </div>
            Handige site werkt super!
          </div>

        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-4 reviewbox text-center">
            <div class="reviewUser">
              <div><img src="/content/images/icons/review1.png" alt="" /></div>
              <div>
                Henk Kuipers <br>
                Helmond
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 reviewbox text-center">
            <div class="reviewUser">
              <div></div>
              <div>
                Anoniem <br>
                Spijkenisse
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 reviewbox text-center">
            <div class="reviewUser">
              <div><img src="/content/images/icons/review3.png" alt="" /></div>
              <div>
                Lilian Verdonschot <br>
                Helden
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <section class="intro-section--call-to-action">
      <div class="title"><h3>Eerder stoppen met werken? Of overzicht krijgen in je pensioen?</h3></div>
      <a id="cta-eerstestap-2" href="#" class="btn btn-cta">regel het zelf</a>
    </section>

    <div class="intro-section--contact">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8">
            <div class="intro-section--contact--direct">
              <h3>Nog ergens hulp bij nodig?</h3>
              <span class="if-generic--phone"></span> <strong>
                <a href="tel:0031356265544">
                  035 - 626 55 44
                </a>
              </strong>
              <p>
                <br>
              </p><div class="text-purple">Rowayna, Edwin en Katinka samen 8 jaar Independer</div>
              <p></p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="intro-section--contact--social">
              <h3>Direct een vraag stellen?</h3>
              <p>Stel je vraag ook aan ons via:</p>
              <div class="clearfix">
                <a href="https://www.facebook.com/Independer.nl/" target="_blank" class="if-generic--facebook"></a>
                <a href="https://twitter.com/independer" target="_blank" class="if-generic--twitter"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include ('footer.php'); ?>
