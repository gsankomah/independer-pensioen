<?php
// ////////////
// PAGE 4
// ////////////

// Wachtscherm, zorgt ervoor dat de gebruiker DOOR kan in de flow. 1 = true
$wachtschermDone = 1;

// Switches
$beleggen = 1;
$sparen = 1;
$beleggenFiscaal = 0;
$sparenFiscaal = 0;
$aflossen = 1;

// -- ALGEMEEN --

// Wachtscherm
$wachtschermText = "Wij maken je pensioenvoorstel. Dit kost een aantal minuten.";

// Expert
//$expertText = "Met je huidige inkomsten, uitgaven en pensioensopbouw lijkt het erop dat je op je ".$gewenstePensioenleeftijd."e €300 per maand minder te besteden hebt. Waarschijnlijk heb je op je 73e genoeg pensioensopbouw om per maand evenveel uit geven als nu.";
$expertText = "Met je huidige inkomsten, uitgaven en pensioensopbouw lijkt het erop dat je op je 62e €643 per maand minder te besteden hebt.";

// -- CONCLUSIE --
$conclusieHeader = "Om te voorkomen dat je geld tekort komt, kun je dit doen:";


// Beleggen
$beleggenTitle = "Ga €530 per maand beleggen.";
$beleggenText = "
Om op je gewenste pensioenleeftijd met pensioen te kunnen gaan, adviseren wij om een deel van je geld te beleggen. Beleggen brengt risico's met zich mee, maar heeft over de lange termijn altijd een beter resultaat laten zien dan sparen. Daarnaast kan je naarmate je het pensioen nadert het risico van de aandelen afbouwen, waardoor de zekerheid dat je het beoogde doelvermogen haalt groter wordt.
";

//Sparen
$sparenTitle = "Ga €354 per maand sparen.";
$sparenText = "
Wij adviseren je om een deel van je geld op een spaarrekening te zetten. Wanneer je geld op een spaarrekening zet loop je weinig risico om je geld kwijt te raken. En kan je bij je geld wanneer jij wilt.
";

// Beleggen met fiscaal voordeel
$beleggenFiscaalTitle = "Ga €124 beleggen.";
$beleggenFiscaalText = "
Wij adviseren je een deel van je geld vast te zetten in beleggingen tot je pensioenleeftijd. Beleggen brengt risico's met zich mee, maar heeft over de lange termijn altijd een beter resultaat laten zien dan sparen.</p><p> Het voordeel bij fiscaal beleggen is dat je de inleg die je nu doet af kunt trekken van de inkomstenbelasting. Deze inkomstenbelasting betaal je pas tijdens de uitkering van je pensioen, waarbij men doorgaans minder inkomsten heeft en dus minder inkomstenbelasting hoeft te betalen. Het nadeel is dat je je geld 'vast' moet zetten en je er dus niet bij kunt tot het moment dat het geld vrijkomt bij pensionering.
";

//Sparen met fiscaal voordeel
$sparenFiscaalTitle = "Ga €0 sparen.";
$sparenFiscaalText = "
Wij adviseren je een deel van je geld vast te zetten tot je pensioenleeftijd. Wanneer je geld op een spaarrekening zet loop je weinig risico om je geld kwijt te raken. Het voordeel hiervan is dat je de inleg die je nu doet af kunt trekken van de inkomstenbelasting. Deze inkomstenbelasting betaal je pas tijdens de uitkering van je pensioen, waarbij men doorgaans minder inkomsten heeft en dus minder inkomstenbelasting hoeft te betalen. Het nadeel is dat je je geld 'vast' moet zetten en je er dus niet bij kunt tot het moment dat het geld vrijkomt bij pensionering.
";

//Aflossen
$aflossenTitle = "Los €884 per maand af op je hypotheek.";
$aflossenText = "
Wij adviseren je om (extra) af te lossen op je hypotheek. Hierdoor heb je wanneer je met pensioen gaat lagere woonlasten. En dus minder geld nodig om te leven zoals je wilt leven.
";

?>
