<?php
// ////////////
// PAGE 2
// ////////////

// $respondent = "Anne Marie"; // Naam van eerste respondent is Jan Roy

// Expert
$expertTitle = "Overzicht van mijnpensioenoverzicht.nl";
$expertText1 = "Om te bepalen of je kan stoppen met werken als je ".$gewenstePensioenleeftijd." jaar bent hebben we wat gegevens van je nodig. Het is voor jezelf makkelijk om deze info van tevoren alvast op te zoeken:";

$expertText2 = "Zodra we al je gegevens hebben verzameld laten we je zien of je genoeg pensioen opbouwt om te stoppen met werken als je ".$gewenstePensioenleeftijd." bent. Kom je tekort? Dan stellen wij je plan samen om ervoor te zorgen dat je alsnog kan stoppen met werken wanneer jij dat wilt.";

?>
