<?php

$pageTitle = "AOW & Werkgeverspensioen";

ini_set('session.cache_limiter', 'private');
session_start();

if(!empty($_POST)){
  extract($_POST);
  /* //////////////////////////////
  /// Uit gezet wegen overbodig ///
  /////////////////////////////////
  GewenstePensioenleeftijd = $pensioenleeftijd;
  $tz  = new DateTimeZone('Europe/Brussels');
  $GewenstePensioenleeftijd = DateTime::createFromFormat('d-m-Y', $geboortedatum, $tz)
  ->diff(new DateTime('now', $tz))
  ->y;

  <p>Je geboortedatum is: <?echo $geboortedatum?> en je gewenste pensioenleeftijd is: <? echo $pensioenleeftijd?>.</p>
  */

  //print_r($_POST);
  //$_POST = str_replace ( "\\" , "" , $_POST );
  //$_POST = array_map('addslashes', $_POST);

} else {
  // Page 1
  $geboortedatum = "01-02-1986";
  $gewenstePensioenleeftijd = "68";
}
include ('content/text/page2Aow.php');

include ('header.php');

?>

<div class="view-container" itemscope="" itemtype="http://schema.org/FinancialService">
  <span itemprop="name" content="Independer"></span>
  <div ui-view="">
    <div class="container">
      <div class="steps-container">
        <div class="step-item step-item--current" style="width: 50%;">
          <a href="/" class="step-item--icon">
            <span class="icon">1</span>
          </a>
          <div class="step-item--label hidden-xs">
            <a><?php echo $pageTitle ?></a>
          </div>
        </div>
        <div class="step-item step-item--untouched" style="width: 50%;">
          <a class="step-item--icon">
            <span class="icon">2</span>
          </a>
          <div class="step-item--label hidden-xs">
            <a>Eigen vermogen</a>
          </div>
        </div>
        <div class="step-item step-item--untouched" style="width: 50%;">
          <a class="step-item--icon">
            <span class="icon">3</span>
          </a>
          <div class="step-item--label hidden-xs">
            <a>Conclusie</a>
          </div>
        </div>
      </div>
    </div>
    <div class="flow-page-header">
      <div class="container">
        <a class="flow-page-header--terug-link link-arrow-back" href="javascript: history.go(-1)">terug</a>
        <h1><span><?php echo $pageTitle ?></span></h1>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <main class="main-holder" role="main" ui-view="">
          <form action="3-eigen-vermogen.php" method="post" enctype="multipart/form-data">
            <input id="geboortedatumRespondent-hidden" type="hidden" name="geboortedatum" value="<?php echo $geboortedatum ?>">
            <input id="pensioenleeftijdRespondent-hidden" type="hidden" name="gewenstePensioenleeftijd" value="<?php echo $gewenstePensioenleeftijd ?>">
            <section class="input-group">
              <h3 class="input-group--header l-margin-none-top"><?php echo $expertTitle ?></h3>
              <div class="expert-box expert-box__viola expert-box__arrow">
                <p><?php echo $expertText1 ?></p>

                <div class="meer-info--uitklap">
                  <a onclick="toggleDiv('meer-info--content1')">+ Laat zien wat ik nodig heb</a>
                  <div class="meer-info--content" id="meer-info--content1" style="display:none;">
                    <h4 style="margin-top:1em;">Checklist</h4>
                    <ul class="expert-checklist">
                      <li>Je DigiD om te kunnen inloggen bij mijnpensioenoverzicht.nl</li>
                      <li>Je bruto jaarsalaris
                        <label class="tooltip-group--expert">
                          <a class="tooltip-icon">?</a>
                          <input type="checkbox" id="tooltip">
                          <span class="tooltip">Dit staat op je loonstrook. Het gaat om je bruto jaarsalaris, inclusief je vakantiegeld en 13e maand. Bonussen en andere flexibele beloningen tellen niet mee.</span>
                        </label>

                      </li>
                      <li>Het saldo van (pensioen)polissen of (pensioen)spaarrekeningen waarmee je naast de AOW en je werkgeverspensioen inkomen voor later opzij zet of hebt gezet

                        <label class="tooltip-group--expert">
                          <a class="tooltip-icon">?</a>
                          <input type="checkbox" id="tooltip">
                          <span class="tooltip">We bedoelen hiermee al het geld dat je momenteel beschikbaar hebt om voor je pensioen apart te zetten. Dat kan een bedrag op je spaar- of beleggingsrekening zijn, maar dat geld kan ook apart staan in bijvoorbeeld een lijfrentepolis of een deposito</span>
                        </label>

                      </li>
                      <li>Als je een koophuis hebt: de laatste jaaropgaaf van je hypotheekverstrekker(s) en een inschatting van de huidige marktwaarde van jouw woning

                        <label class="tooltip-group--expert">
                          <a class="tooltip-icon">?</a>
                          <input type="checkbox" id="tooltip">
                          <span class="tooltip">De inschatting van de huidige marktwaarde kan je zelf doen; het gaat erom dat we een indicatie hebben</span>
                        </label>

                      </li>
                      <li>Als je andere leningen hebt: het bedrag dat je nog moet afbetalen

                        <label class="tooltip-group--expert">
                          <a class="tooltip-icon">?</a>
                          <input type="checkbox" id="tooltip">
                          <span class="tooltip">Denk aan je studiefinanciering, maar ook aan consumptieve kredieten en afbetalingsregelingen</span>
                        </label>

                      </li>
                    </ul>
                  </div>
                </div>

                <p><?php echo $expertText2 ?></p>
                <div class="expert-box--bottom">
                  <div class="expert-box--expert-title">
                    Viola, <br class="visible-xs">Independer sinds 2004
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">

                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <h4>Stap 2: Download je pensioenoverzicht</h4>
                  <p>Bij mijnpensioenoverzicht.nl wordt deze informatie voor je bijgehouden. Je kan daar inloggen met je DigiD en je overzicht downloaden.</p>
                  <a href="http://www.mijnpensioenoverzicht.nl/" target="_blank" class="btn" style="margin-right:10px;">Download overzicht vanuit mijnpensioenoverzicht.nl</a>
                  <a id="howtoMPO" onclick="$('div#ngdialog1').toggleClass('ngdialog-open')">> hoe werkt dit?</a>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <h4>Stap 3: Upload je pensioenoverzicht</h4>
                  <div class="input-item">
                    <div class="input-item--answer">
                      <p>Kies het pdf-bestand dat je net hebt gedownload.</p>
                    <p>
                      <!-- <a id="mpoUpload" class="btn" onclick="$('div#experimentdialog').toggleClass('ngdialog-open')">kies het pdf bestand om te uploaden</a> -->
                      <a id="mpoUpload" class="btn manual-optin-trigger" data-optin-slug="qmbpi2aep53adxio" onClick="ga('send', 'event', 'show-popup', 'click');">kies het pdf-bestand om te uploaden</a>
                      <input type="file" required="" name="file" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple />
                      <label><!--<strong class="btn">kies het pdf bestand om te uploaden</strong> -->
                      <span></span>
                      </label>
                    </p>
                </div>
            </div>
                </div>
              </div>
            </section>
            <div class="buttons-container text-right">
              <button id="submitForm" type="submit" class="btn btn-cta btn-mid manual-optin-trigger" data-optin-slug="qmbpi2aep53adxio" onClick="ga('send', 'event', 'show-popup', 'click');">verder</button>
            </div>
        </form>
      </main>
      <aside class="aside-holder" role="complementary">
        <div class="feedback-aside">
            <h4>Jouw gegevens:</h4>
            <img class="feedback-aside--img">
            Jouw geboortedatum: <?php echo $geboortedatum ?> <br>
            Gewenste pensioenleeftijd: <?php echo $gewenstePensioenleeftijd ?>
          </div>
          <div class="contact-block-sidebar ng-isolate-scope">
            <img src="content/images/klant-contact2.png">
            <div class="contact-block-sidebar-inner clearfix">
              <p class="small text-purple">Hetty, Thijs en Adrian<br>Samen 19 jaar Independer</p>
              <div class="media media-small">
                <div class="media--img">
                  <span class="if-generic--phone icon-color-green"></span>
                </div>
                <div class="media--content">
                  <div class="text-bold">
                    <a class="link-phone" href="tel:+31356265544">035 - 626 55 44</a>
                  </div>
                  <span>vandaag tot 21:00 uur</span>
                </div>
              </div>
              <div class="l-margin-20-top">
                <p>Stel je vraag ook aan ons via:</p>
                <div class="contact-block-horizontal--social-links">
                  <a href="https://www.facebook.com/Independer.nl" target="_blank" class="if-generic--facebook"></a>
                  <a href="https://twitter.com/independer" target="_blank" class="if-generic--twitter"></a>
                  <a href="mailto:info@independer.nl" target="_blank" class="if-generic--mail"></a>
                </div>
              </div>
            </div>
          </div>
        </aside>
      </div>
    </div>
  </div>
  <!-- Experiment dialog -->
  <!-- <div id="experimentdialog" class="ngdialog ngdialog-popup">
    <div class="ngdialog-overlay" onclick="$('div#experimentdialog').toggleClass('ngdialog-open')"></div>
    <div role="document" class="ngdialog-content">
      <div class="ngdialog-title" style="height: 71px;">
        Titel
      </div>
      <div class="ngdialog-content">
        <form class="" action="" method="post">
          <div class="input-group">
            Email veld
            Submit button
          </div>
        </form>
      </div>
    </div>
  </div> -->

  <!-- Hoe werkt het dialog -->
  <div class="ngdialog ngdialog-side-panel" id="ngdialog1">
    <div class="ngdialog-overlay" onclick="$('div#ngdialog1').toggleClass('ngdialog-open')"></div>
    <div role="document" class="ngdialog-content">
      <div ng-transclude="" class="ngdialog-title" style="height: 71px;">
        Hoe werkt dit <span>Mijnpensioenoverzicht.nl</span>
      </div>
      <div class="ngdialog-content-scroll" style="top: 71px;">
        <div>
          Omdat we je het zo gemakkelijk mogelijk willen maken bij het invoeren van je pensioen, hebben wij een overzicht nodig vanuit mijnpensioenoverzicht.nl. Hieronder staat per stap omschreven hoe je aan dit overzicht kunt komen. Is er iets niet duidelijk? Neem dan gerust contact met ons op!
          <p>&nbsp;</p>
        </div>

        <h2>Stap 1</h2>
        <h5>Open www.mijnpensioenoverzicht.nl</h5>
        <p>Ga naar www.mijnpensioenoverzicht.nl om de pagina te openen.</p>

        <h2>Stap 2</h2>
        <h5>Klik op ‘Bekijk mijn pensioenoverzicht’</h5>
        <p>Onderin de website vind je een rode knop waarop staat ‘Bekijk mijn pensioenoverzicht’, klik hierop om naar de volgende pagina te gaan.</p>

        <img src="content/images/tutorials/mijnpensioenoverzicht/picture1.png">

        <h2>Stap 3</h2>
        <h5>Log in met je DigiD gegevens</h5>
        <p>Je DigiD gegevens zijn nodig om mijnpensioenoverzicht.nl toegang te geven tot jouw opgebouwde pensioen.</p>

        <img src="content/images/tutorials/mijnpensioenoverzicht/picture2.png">

        <h2>Stap 4</h2>
        <h5>Klik op ‘Bekijk mijn pensioenoverzicht’</h5>
        <p>Vervolgens kom je in onderstaand scherm. Vul hier in of je een partner hebt of alleenstaand bent. Dit heeft namelijk gevolgen voor de hoogte van je AOW. Druk vervolgens op ‘bekijk mijn pensioenoverzicht’ onderin de pagina.</p>

        <img src="content/images/tutorials/mijnpensioenoverzicht/picture3.png">

        <h2>Stap 5</h2>
        <h5>Klik volgende tot je aankomt op ‘Mijn te bereiken pensioen’ zoals hieronder</h5>
        <p>Op deze pagina zie je rechtsbovenin "Download gegevens of samenvattingen", klik hierop om jouw pensioenoverzicht.</p>

        <img src="content/images/tutorials/mijnpensioenoverzicht/picture4.png">

        <h2>Stap 6</h2>
        <h5>Sla je pensioen op als PDF-bestand</h5>
        <p>Je kunt nu je pensioenoverzicht downloaden als PDF-bestand. Dit bestand hebben wij nodig om je overzicht te maken en een pensioen voorstel te maken</p>

        <img src="content/images/tutorials/mijnpensioenoverzicht/picture5.png">


      </div>

      <div class="ngdialog-close" onclick="$('div#ngdialog1').toggleClass('ngdialog-open')">x</div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $( document ).ready(function() {
    var geboortedatumRespondent = document.getElementById("geboortedatumRespondent-hidden").value;
    var pensioenleeftijdRespondent = document.getElementById("pensioenleeftijdRespondent-hidden").value;

    heap.addUserProperties({'Geboortedatum': geboortedatumRespondent, 'Pensioenleeftijd': pensioenleeftijdRespondent});
  })
</script>

<?php include ('footer.php'); ?>
