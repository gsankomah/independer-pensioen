<!DOCTYPE html>
<html lang="nl">
<head>
  <meta charset="UTF-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

  <title><?php echo "$pageTitle | Independer - Verzekeringen vergelijken en afsluiten"; ?></title>

  <!-- Google Analytics -->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-82059629-1', 'auto');
    ga('send', 'pageview');
  </script>

    <!-- Heap Analytics -->
  <script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("3792427941");
  </script>

    <!-- <script src="//load.sumome.com/" data-sumo-site-id="a059a59e0626dde246d45b057c1175e8ec98739a5a0c0cad3efce0114bd87e1f" async="async"></script> -->

  <!-- Hotjar Tracking Code for http://www.pensioen.independer.nl -->
  <script>
      (function(h,o,t,j,a,r){
          h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
          h._hjSettings={hjid:262484,hjsv:5};
          a=o.getElementsByTagName('head')[0];
          r=o.createElement('script');r.async=1;
          r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
          a.appendChild(r);
      })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
  </script>

  <!-- OptinMonster -->
  <div id="om-qmbpi2aep53adxio-holder"></div><script>var qmbpi2aep53adxio,qmbpi2aep53adxio_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){qmbpi2aep53adxio_poll(function(){if(window['om_loaded']){if(!qmbpi2aep53adxio){qmbpi2aep53adxio=new OptinMonsterApp();return qmbpi2aep53adxio.init({"u":"21828.390313","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmnstr.com/app/js/api.min.js",o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;qmbpi2aep53adxio=new OptinMonsterApp();qmbpi2aep53adxio.init({"u":"21828.390313","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script>

  <!-- Optimizely -->
  <script src="https://cdn.optimizely.com/js/6835320601.js"></script>

  <!-- Reactful Plugin -->
  <script type="text/javascript">
    (function() {
      var reactful_client_id="879035";
      window._rctfl = window._rctfl || {c: []}; window._rctfl.c.push(reactful_client_id);
      var el = document.createElement('script'); el.async = 1;
      el.src = "//visitor.reactful.com/dist/main.rtfl.js";
      document.getElementsByTagName('head')[0].appendChild(el);
    })();
  </script>
  <!-- End Reactful Plugin -->

  <link href="content/css/css-independer.css" rel="stylesheet"/>
  <link href="content/css/css-independer2.css" rel="stylesheet"/>

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
</head>
<body itemscope="" itemtype="http://schema.org/WebPage">
  <header id="pageHeader" class="page-header page-header-minimal" itemtype="http://schema.org/WPHeader">
    <div id="pageHeaderTop" class="page-header--top">
      <div class="container">
        <div class="pull-right">
          <div class="page-header--phonenumber">
            <a class="link-phone" href="tel:+31356265544"><span class="if-generic--phone"></span> <strong><span class="text">035 - 626 55 44</span></strong></a>
            <span class="hidden-xs">vandaag tot 21:00 uur</span>
          </div>
          <!-- <a class="page-header--search"><span class="if-generic--search"></span></a> -->
          <a class="page-header--help"><span class="if-generic--help-baloon"></span><span class="text">Help mij</span></a>
        </div>

        <a class="page-header--logo-link" href="/" itemprop="name"><div class="page-header--logo-container"><img class="page-header--logo" src="content/images/independer_logo-on-purple-rgb.svg" alt="Independer"></div></a>
      </div>
    </div>
  </header>
